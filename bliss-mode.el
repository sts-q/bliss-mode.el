;;=============================================================================
;;
;; :file:   bliss-mode.el               emacs syntax highlighting for bliss
;;
;;=============================================================================
;;
;;     -*- mode: bliss -*-
;;
;;=============================================================================
;; :section:   bliss-font-lock-function
;;-----------------------------------------------------------------------------

(defun bliss-font-lock-function()
  "return a ( regexp . color ) list"
  (let* ((head   "\\<")                
         (tail   "\\>")
         (word   "[[:word:]]+")
         (faces 
          `(
            
            ;; ---------------------------------------------------------
            ;; MODULE name =
             (,(concat "\\(" head "MODULE" tail "\\)\\s-*\\(" word "\\)\\s-*=")
              (1  font-lock-keyword-face)
              (2  font-lock-function-name-face))

            ;; ROUTINE name
             (,(concat "\\(" head "GLOBAL ROUTINE" tail "\\)\\s-*\\(" word "\\)")
              (1  font-lock-keyword-face)
              (2  font-lock-function-name-face))

            ;; ROUTINE name
             (,(concat "\\(" head "ROUTINE" tail "\\)\\s-*\\(" word "\\)")
              (1  font-lock-keyword-face)
              (2  font-lock-function-name-face))

            ;; ---------------------------------------------------------
            ;; warning
            (,(concat head (regexp-opt
                            '("tron" "troff"
                ) t) tail)      .  font-lock-warning-face)
            
            ;; keywords
            (,(concat head (regexp-opt '(
                "LIBRARY"
                "ELUDOM"
                ) t) tail)      .  font-lock-keyword-face)

            ;; keywords
            (,(concat head (regexp-opt '(
                "GLOBAL"
                "LOCAL"
                "LABEL"
                "OWN"
                ) t) tail)      .  font-lock-comment-face)
            
            ;; BEGIN END
            (,(concat head (regexp-opt '(
                "BEGIN" "END"
                ) t) tail)      .  font-lock-comment-face)

            ;; ---------------------------------------------------------
            ;; builtin keywords
            (,(concat head (regexp-opt '(
                "NOVALUE"
                "IF" "THEN" "ELSE"
                "INCR" "DECR" "FROM" "TO" "DO" "BY"
                "WHILE" "UNTIL"
                "EXITLOOP" "LEAVE" "RETURN"
                "CASE" "SELECTONE" "SELECT" "OF" "SET" "TES" "OTHERWISE" "ALWAYS"
                "VECTOR" "INITIAL" "UPLIT"
                "BITVECTOR"
                "FIELD" "BLOCK" "REF"
                "BYTE" "WORD"
                "BIND"
                ) t) tail)      .  font-lock-comment-face)
            
            ;; ---------------------------------------------------------
            ;; builtin funtions
            (,(concat head (regexp-opt '(
                "EQL" "GEQ" "LEQ" "LSS" "GTR"
                "AND" "OR" "NOT" "XOR" "EQV" "EQLU" "EQLA"
                "MOD" 
                ) t) tail)  .  font-lock-comment-face)
            
            
            ;; ---------------------------------------------------------
            ;; numbers and interpunction
            (,(concat head (regexp-opt
                '("true" "false"
                ) t) tail)  .  font-lock-constant-face)

            (,(concat  "[[();,^:=]" )            .  font-lock-comment-face)
            (,(concat  "]" )                     .  font-lock-comment-face)
            (,(concat  "[{}]" )                  .  font-lock-warning-face)
            
            (,(concat head        "\\-[0-9]+" tail)      .  font-lock-builtin-face)
            (,(concat head     "[\\+]?[0-9]+" tail)      .  font-lock-constant-face)
            ;; ---------------------------------------------------------
            (,(concat head    "\\."word tail)      .  font-lock-type-face)
            
            )))
    faces))

;;=============================================================================

(defvar bliss-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<return>") 'align-newline-and-indent)
    map))

(define-derived-mode   bliss-mode prog-mode "bliss" 
  "Define major mode for editing bliss files."
  (setf font-lock-defaults       (list (bliss-font-lock-function)))
)

(add-to-list 'auto-mode-alist '("\\.bli\\'" . bliss-mode))

(add-hook 'bliss-mode-hook
          (lambda ()
;;          (setq-local indent-line-function 'bliss-indent-line-function)
            ))

(mapcar (lambda (p)
          (modify-syntax-entry (car p) (cadr p) bliss-mode-syntax-table))
        '(
                                        
          (?_   "W" )           ; take _.+- as word chars
          (?.   "W" )
          (?+   "w" )
          (?-   "w" )
          
          (?'    "\"")          ; "'" is string delimiter: highlight 'x' like string "x"
;;        (?`    "\"")          ; "`" is string delimiter: highlight `x` like string "x"

          (?!  "< b")           ; ! starts comment
          (?\n "> b")           ; comment up to end of line

          ))

;; (* ... *) - comment
;;(modify-syntax-entry ?(  ". 1"   bliss-mode-syntax-table)      ; ( is first char of comment
;;(modify-syntax-entry ?*  ". 23"  bliss-mode-syntax-table)      ; * second and third char in (**)
;;(modify-syntax-entry ?)  ". 4"   bliss-mode-syntax-table)      ; ) ends comment

(provide 'bliss-mode)
;;=============================================================================

