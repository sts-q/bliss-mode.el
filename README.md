# README #

# bliss-mode.el #

## Emacs syntax highlighting for the BLISS programming language ##

BLISS is a system programming language developed at Carnegie Mellon University by 
W. A. Wulf, D. B. Russell, and A. N. Habermann around 1970.

[BLISS-at-wikipedia](https://en.wikipedia.org/wiki/BLISS)

[BLISS compiler project](https://madisongh.github.io/blissc/)
